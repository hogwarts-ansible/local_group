# local_group

This role creates group based on local_group_group hash, if defined.
Otherwise, no group will be created

# Example inventory

```yaml
---
local_group_group:
  foobar:
    gid: 700
    state: present
    system: true
  barfoo: null
  _group999$:
    state: absent
```

# Example playbook

```yaml
---
- name: Create local groups
  hosts: all
  roles:
    - role: local_group
```

# License

GPLv3

# Author information

Wallun <wallun AT disroot DOT org>
